
#include "test.h"
#include <stdio.h>

int main() {
    if (test1()) {
        printf("Test 1 passed\n");
    } else {
        printf("Test 1 failed\n");
        return 1;
    };
    if (test2()) {
        printf("Test 2 passed\n");
    } else {
        printf("Test 2 failed\n");
        return 1;
    };
    if (test3()) {
        printf("Test 3 passed\n");
    } else {
        printf("Test 3 failed\n");
        return 1;
    };
    if (test4()) {
        printf("Test 4 passed\n");
    } else {
        printf("Test 4 failed\n");
        return 1;
    };
    if (test5()) {
        printf("Test 5 passed\n");
    } else {
        printf("Test 5 failed\n");
        return 1;
    };


    return 0;
}
