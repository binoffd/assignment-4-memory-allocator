//
// Created by void on 3/2/23.
//
#include <stdio.h>
#define __USE_MISC 1
#include "mem_internals.h"
#include "mem.h"
#include "test.h"
#define assert(x) if (!(x)) return false;
#define COUNT_OF_MALLOC 100
static struct block_header* get_header(void* content) {
    return (struct block_header*) (content - offsetof(struct block_header, contents));
}

// Тест №1: Обычное успешное выделение памяти.
bool test1() {
    const size_t heap_size = capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes;
    void* heap = heap_init(heap_size);
    void* block = _malloc(COUNT_OF_MALLOC);
    struct block_header *header = get_header(block);

    assert(block != NULL);
    assert(heap != NULL);
    assert(header->capacity.bytes != COUNT_OF_MALLOC);

    _free(block);
    munmap(heap, heap_size);

    return true;
}

// Тест №2: Освобождение одного блока из нескольких выделенных.
bool test2() {
    const size_t heap_size = capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes;
    void* heap = heap_init(heap_size);
    void* block1 = _malloc(COUNT_OF_MALLOC);
    void* block2 = _malloc(COUNT_OF_MALLOC);
    void* block3 = _malloc(COUNT_OF_MALLOC);

    assert(heap != NULL);
    assert(block1 != NULL);
    assert(block2 != NULL);
    assert(block3 != NULL);

    size_t new_block_size = size_from_capacity(get_header(block3)->capacity).bytes + COUNT_OF_MALLOC;

    _free(block3);

    assert(get_header(block3)->is_free);
    assert(get_header(block3)->capacity.bytes == new_block_size);

    _free(block1);
    _free(block2);
    munmap(heap, heap_size);

    return true;
}

// Тест №3: Освобождение двух блоков из нескольких выделенных.
bool test3() {
    const size_t heap_size = capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes;
    void* heap = heap_init(heap_size);
    void* block1 = _malloc(COUNT_OF_MALLOC);
    void* block2 = _malloc(COUNT_OF_MALLOC);
    void* block3 = _malloc(COUNT_OF_MALLOC);
    void* block4 = _malloc(COUNT_OF_MALLOC);

    assert(heap != NULL);
    assert(block1 != NULL);
    assert(block2 != NULL);
    assert(block3 != NULL);
    assert(block4 != NULL);

    size_t new_block_size = size_from_capacity(get_header(block3)->capacity).bytes + size_from_capacity(get_header(block4)->capacity).bytes;
    _free(block3);
    _free(block2);

    assert(get_header(block2)->is_free);
    assert(get_header(block3)->is_free);
    assert(get_header(block2)->capacity.bytes == new_block_size);

    _free(block1);
    _free(block4);
    munmap(heap, heap_size);

    return true;
}

// Тест №4: Память закончилась, новый регион памяти расширяет старый.
bool test4() {
    const size_t heap_size = capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes;
    void* heap = heap_init(heap_size);
    void* block1 = _malloc(heap_size);

    assert(heap != NULL);
    assert(block1 != NULL);
    assert(get_header(block1)->capacity.bytes == REGION_MIN_SIZE);
    assert(get_header(block1)->next == NULL);

    void* block2 = _malloc(COUNT_OF_MALLOC);

    assert(block2 == block1 + REGION_MIN_SIZE + offsetof(struct block_header, contents));
    assert(get_header(block1)->next == get_header(block2));

    _free(block1);
    _free(block2);
    munmap(heap, heap_size);

    return true;
}

// Тест №5: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион
bool test5() {
    const size_t heap_size = capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes;
    void* heap = heap_init(heap_size);
    void* block1 = _malloc(heap_size);

    assert(heap != NULL);
    assert(block1 != NULL);
    assert(get_header(block1)->capacity.bytes == REGION_MIN_SIZE);
    assert(get_header(block1)->next == NULL);


    // Вручную занять последующий диапозон адресов чтобы мы не смогли расширять старый
    void* p = mmap(get_header(block1)->contents + get_header(block1)->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);

    assert(p != MAP_FAILED);

    void* block2 = _malloc(REGION_MIN_SIZE);

    assert(block2 != NULL);
    assert(block2 != block1 + get_header(block1)->capacity.bytes);

    _free(block1);
    _free(block2);
    munmap(heap, heap_size);
    munmap(p, REGION_MIN_SIZE);

    return true;
}
